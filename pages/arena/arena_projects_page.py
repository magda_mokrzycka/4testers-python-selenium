from selenium.webdriver.common.by import By


class ArenaProjectsPage:

    def __init__(self, browser):
        self.browser = browser

    def verify_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def click_add_project_button(self):
        add_project_button = self.browser.find_elements(By.CSS_SELECTOR, '.button_link')[0]
        add_project_button.click()

    def insert_message(self, text):
        self.browser.find_element(By.CSS_SELECTOR, 'input#search').send_keys(text)

    def click_search_icon(self):
        icon_button = self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton')
        icon_button.click()

    def verify_created_project_by_name(self, name):
        listElements = self.browser.find_elements(By.CSS_SELECTOR, 'tr td')
        found = False

        for element in listElements:
            if element.text == name:
                found = True
                break

        assert found



from selenium.webdriver.common.by import By

from utils.random_message import generate_random_text


class ArenaAddProject:

    def __init__(self, browser):
        self.browser = browser

    def verify_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def verify_url(self, project):
        assert self.browser.current_url == project

    def insert_message(self, selector, random_text):
        self.browser.find_element(By.CSS_SELECTOR, selector).send_keys(random_text)

    def click_save_project_button(self):
        add_project_button = self.browser.find_element(By.CSS_SELECTOR, 'input#save')
        add_project_button.click()

    def is_project_add_confirmed(self):
        assert self.browser.find_element(By.CSS_SELECTOR, '#j_info_box').is_displayed()

    def click_section_projects_button(self):
        section_projects_button = self.browser.find_element(By.CSS_SELECTOR, 'li.item2')
        section_projects_button.click()

import time

import pytest
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

from pages.arena.arena_add_project import ArenaAddProject
from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login import ArenaLoginPage
from pages.arena.arena_projects_page import ArenaProjectsPage
from utils.random_message import generate_random_text


@pytest.fixture
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    driver.quit()


def test_should_add_project_correctly_flow(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()

    arena_projects_page = ArenaProjectsPage(browser)
    arena_projects_page.verify_title('Projekty')
    arena_projects_page.click_add_project_button()

    arena_add_project = ArenaAddProject(browser)
    arena_add_project.verify_url("http://demo.testarena.pl/administration/add_project")

    name = generate_random_text(12)
    arena_add_project.insert_message('input#name', name)
    prefix = generate_random_text(7)
    arena_add_project.insert_message('input#prefix', prefix)
    description = generate_random_text(50)
    arena_add_project.insert_message('textarea#description', description)

    arena_add_project.click_save_project_button()
    arena_add_project.is_project_add_confirmed()
    arena_add_project.click_section_projects_button()

    arena_projects_page.verify_title('Projekty')
    arena_projects_page.insert_message(name)
    arena_projects_page.click_search_icon()
    arena_projects_page.verify_created_project_by_name(name)

    time.sleep(4)
